function PostCode(postdata) {
    // An object of options to indicate where to post to
    var post_options = {
        host: '192.168.1.49',
        port: '8088',
        path: '/NewProd/SaveProd',
        method: 'POST',
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded',
            'Content-Length': Buffer.byteLength(postdata)
        }
    };

    // Set up the request
    var post_req = http.request(post_options, function(res) {
        res.setEncoding('utf8');
        res.on('data', function(chunk) {
            console.log('Response: ' + chunk);
        });
    });

    // post the data
    post_req.write(postdata);
    post_req.end();
}


function PostTest(postdata) {
    console.log("ZZZZZZZZZZ" + postdata);
    requestify.post('192.168.1.49:8088/NewProd/SaveProd', { es: postdata })
        .then(function(response) {
            // Get the response body (JSON parsed or jQuery object for XMLs)
            console.log(response.getBody());
            // Get the raw response body
            response.body;
        })
        .fail(function(response) {
            console.log(response.getCode());
        });
}


function reqPostTest(postdata) {
    request.post(
        '192.168.1.49:8088/NewProd/SaveProd', { es: postdata },
        function(error, response, body) {
            if (!error && response.statusCode == 200) {
                console.log(body)
            }
            console.log(body);
        }
    );
}