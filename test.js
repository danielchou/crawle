var request = require("request");
var fs = require("fs");
var cheerio = require("cheerio");
var http = require("http");
var qs = require("querystring");
var async = require('async'); //同步
var requestify = require('requestify'); //POST
var needle = require('needle'); //POST

var webUrl = "http://www.ez66.com.tw/tw/";
//var typs = ["助行車", "沐浴泡湯", "美髮染整", "口腔保健"];
//var typs = ["手杖", "輪椅", "其他行動輔具", "放大鏡", "閱讀輔助", "聽力輔助", "通訊產品", "床及週邊", "枕頭床墊", "被套單墊", "照護館"];
//var typs = ["餐廚館", "調理用品", "家具館", "浴室用品", "衛生用品", "扶手類", "防滑防撞類", "出入類", "個人安全", "居家安全", "電器館", "護甲修容", "沐浴清潔", "護膚美顏","護具類","能量精品","冷熱敷墊","醫療用品","健康管理","其他保健","男內著","女內著","帽子","配件包包","男裝","女裝"];
var typs = ["男鞋", "女鞋", "室內鞋", "襪子", "鞋具鞋墊", "健身器材", "益智遊戲", "運動配件", "按摩用品", "美勞創作", "雜貨館", "cd", "dvd", "生機天然飲食", "保健養生食品", "素食", "金孫館", "禮品館"];
var pageSize = 15;
var DBAPIUrl = "http://192.168.1./NewProd/SaveProd";

///test///http://www.ez66.com.tw/tw/%E8%B2%B7dunlop-%E9%81%8B%E5%8B%95%E9%9E%8B-%E5%A5%BD%E7%A6%AE2%E9%81%B81
///test///http://www.ez66.com.tw/tw/%E7%A6%AE%E5%93%81%E9%A4%A8

// CrawlNum();
for (var i = 0; i < typs.length; i++) {
    var typName = typs[i];
    seriesFunc(typName);
}


function seriesFunc(typs) {
    async.waterfall([
        function(callback) {
            console.log("Step 1：")
            var MaxPageNum = 0
            var url = webUrl + encodeURI(typs);
            CrawlNum(url, function(number) {
                //console.log(number);
                MaxPageNum = number;
                if (MaxPageNum == 0) {
                    MaxPageNum = 1;

                }
                console.log("max num:" + MaxPageNum)
                callback(null, MaxPageNum);
            });
        },
        function(data, callback) {
            console.log("Step 2：");
            for (var i = 1; i <= data; i++) {
                CrawlWeb(typs, i, function(rs) {
                    // console.log(rs);
                }, print);
            }
        }
    ]);
}

// use this!!
function needPostTest(postdata) {
    needle.post('localhost:63088/NewProd/SaveProd', { es: postdata },
        function(err, resp, body) {
            console.log(resp);
        });
}


function CrawlNum(url, pageNum) {
    var num = 0;
    request(url, function(err, res, body) {
        if (err) console.log('Erro: ' + err);
        var $ = cheerio.load(body);
        var items = $('.individual-page')
        for (var i = 0; i < items.length; i++) {
            var item = items[i]
            var n = $(item).find("a").text();
            num = n;
        }
        pageNum(num);
    });

}



function CrawlWeb(typName, pageNum, pp, cb) {
    var uri = encodeURI(typName);
    var rs;
    var result = [];
    var temp = "";

    request({
        url: webUrl + uri + "?pagesize=" + pageSize + "&pagenumber=" + pageNum,
        method: "GET"
    }, function(e, r, b) {
        if (e || !b) {
            return;
        }

        // PageNum =?

        var $ = cheerio.load(b);
        //var result = [];
        var items = $(".product-grid .item-box");
        for (var i = 0; i < items.length; i++) {

            var item = items[i];
            var img = $(item).find(".product-item .picture a"),
                pic = $(img).find("img").attr("src"),
                imgalt = $(img).attr("title");
            var details = $(item).find(".details"),
                dscr = $(details).find(".description").text().replace("\r\n", "").trim(),
                prices = $(details).find(".add-info .prices"),
                oldprice = $(prices).find(".old-price").text().replace("$", "").replace("\r\n", "").replace(",", "").trim(),
                price = $(prices).find(".actual-price").text().replace("$", "").replace(",", "").replace(" 起", "");

            result.push({
                "Typ1": typName,
                "PicUrl": pic,
                //"ImgAlt": imgalt,
                "Name": imgalt,
                "dscr": dscr,
                "OldPrice": oldprice,
                "Price": price
            });
        }
        //temp = JSON.stringify(result);
        console.log(result);
        pp(pageNum + ":" + temp);
        cb(result);
    });



}



function print(data) {
    // console.log(data);   // post寫這
    // PostCode("{es:"+data+"}");
    // PostTest(data);
    // reqPostTest(data);
    needPostTest(data);
}